/* global describe it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

const app = require('../../app');

describe('App', () => {
  describe(`GET /unknown_path`, () => {
    it('should return 404 when route is not found', () => {
      return chai
        .request(app)
        .get(`/unknown_path`)
        .then((res) => {
          const body = res.body;

          expect(res.status).to.be.equal(404);
          expect(body).to.be.a('object');
          expect(body.code).to.equal(404);
          expect(body.message).to.equal('NotFound');
          expect(body.errors).to.equal(`Route not found`);
        })
        .catch((err) => {
          throw new Error(err);
        });
    });
  });
});
