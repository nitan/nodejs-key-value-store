/* global describe beforeEach it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

const app = require('../../../app');
const Obj = require('../../../app/models/object');
const BASE_URL = '/object';

describe(`GET ${BASE_URL}/:key`, () => {
  const objects = [
    {
      key: 'my-key-1',
      value: 'Jan 1, 2018',
      timestamp: 1514764800 // 2018-01-01 00:00:00
    },
    {
      key: 'my-key-1',
      value: 'Jan 2, 2018',
      timestamp: 1514851200 // 2018-01-02 00:00:00
    }
  ];

  beforeEach(async () => {
    try {
      await Obj.remove({}).exec();
      await Obj.create(objects);
    } catch (err) {
      throw new Error(err);
    }
  });

  it('should GET the latest object if no timestamp is provided', () => {
    const expected = objects[1];
    return chai
      .request(app)
      .get(`${BASE_URL}/${expected.key}`)
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(200);
        expect(body).to.be.a('object');
        expect(body.key).to.equal(expected.key);
        expect(body.value).to.equal(expected.value);
        expect(body.timestamp).to.equal(expected.timestamp);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });

  it('should GET the latest object earlier to the timestamp provided', () => {
    const expected = objects[0];
    const timestamp = 1514764801; // 2018-01-01 00:00:01
    return chai
      .request(app)
      .get(`${BASE_URL}/${expected.key}`)
      .query({ timestamp })
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(200);
        expect(body).to.be.a('object');
        expect(body.key).to.equal(expected.key);
        expect(body.value).to.equal(expected.value);
        expect(body.timestamp).to.equal(expected.timestamp);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });

  it('should return 404 when object with key is not found', () => {
    const unknownKey = 'unknown-key';
    return chai
      .request(app)
      .get(`${BASE_URL}/${unknownKey}`)
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(404);
        expect(body).to.be.a('object');
        expect(body.code).to.equal(404);
        expect(body.message).to.equal('NotFound');
        expect(body.errors).to.equal(`Object with key: ${unknownKey} not found`);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });

  it('should return 404 when object with timestamp is not found', () => {
    const timestamp = 1514764799; // 2017-12-31 23:59:59
    return chai
      .request(app)
      .get(`${BASE_URL}/${objects[0].key}`)
      .query({ timestamp })
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(404);
        expect(body).to.be.a('object');
        expect(body.code).to.equal(404);
        expect(body.message).to.equal('NotFound');
        expect(body.errors).to.equal(`Object with key: ${objects[0].key} not found`);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });

  it('should return 400 when key is empty (%20 for space)', () => {
    const spaces = '%20%20';

    return chai
      .request(app)
      .get(`${BASE_URL}/${spaces}`)
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(400);
        expect(body).to.be.a('object');
        expect(body.code).to.equal(400);
        expect(body.message).to.equal('ValidationError');
        expect(body.errors.length).to.equal(1);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });
});
