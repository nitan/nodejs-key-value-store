/* global describe it */
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

const app = require('../../../app');
const BASE_URL = '/object';

describe(`POST ${BASE_URL}`, () => {
  it('should POST an object', () => {
    let object = { key: 'value' };

    return chai.request(app)
      .post(`${BASE_URL}`)
      .send(object)
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(201);
        expect(body).to.be.a('object');
        expect(body.key).to.equal('key');
        expect(body.value).to.equal(object.key);
        expect(body.timestamp).to.greaterThan(0);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });

  it('should not POST an object with multiple keys', () => {
    let object = {
      key1: 'value',
      key2: 'value2'
    };

    return chai.request(app)
      .post(`${BASE_URL}`)
      .send(object)
      .then((res) => {
        const body = res.body;

        expect(res.status).to.equal(400);
        expect(body).to.be.a('object');
        expect(body.code).to.equal(400);
        expect(body.message).to.equal('ValidationError');
        expect(body.errors.length).to.equal(1);
      })
      .catch((err) => {
        throw new Error(err);
      });
  });
});
