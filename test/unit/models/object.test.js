/* global describe beforeEach it */

const chai = require('chai');
const expect = chai.expect;

const Obj = require('../../../app/models/object');

describe('Object', () => {
  describe('#preSave', () => {
    it('should add timestamp if not provided', async () => {
      try {
        const newObject = new Obj({
          key: 'ABC',
          value: '123'
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('timestamp');
        expect(savedObject.timestamp).to.be.a('number');
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should not override timestamp if provided', async () => {
      try {
        const newObject = new Obj({
          key: 'ABC',
          value: '123',
          timestamp: 1514764800 // 2018-01-01 00:00:00
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('timestamp');
        expect(savedObject.timestamp).to.equal(newObject.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });
  });

  describe('#save', () => {
    it('should return saved object', async () => {
      try {
        const newObject = new Obj({
          key: 'ABC',
          value: '123',
          timestamp: 1514764800 // 2018-01-01 00:00:00
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('_id');
        expect(savedObject.key).to.equal(newObject.key);
        expect(savedObject.value).to.equal(newObject.value);
        expect(savedObject.timestamp).to.equal(newObject.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should strip values not in the schema', async () => {
      try {
        const newObject = new Obj({
          key: 'ABC',
          value: '123',
          unknownKey: 'Key not in schema'
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.not.have.property('unknownKey');
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should trim whitespaces for key', async () => {
      try {
        const objWithSpaces = new Obj({
          key: ' With Spaces ',
          value: 'value'
        });

        const savedObject = await objWithSpaces.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('_id');
        expect(savedObject.key).to.equal(objWithSpaces.key.trim());
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should accept unicode characters', async () => {
      try {
        const newObject = new Obj({
          key: '汉字',
          value: '汉字',
          timestamp: 1514764800 // 2018-01-01 00:00:00
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('_id');
        expect(savedObject.key).to.equal(newObject.key);
        expect(savedObject.value).to.equal(newObject.value);
        expect(savedObject.timestamp).to.equal(newObject.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should accept null value', async () => {
      try {
        const newObject = new Obj({
          key: 'key',
          value: null,
          timestamp: 1514764800 // 2018-01-01 00:00:00
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('_id');
        expect(savedObject.key).to.equal(newObject.key);
        expect(savedObject.value).to.equal(newObject.value);
        expect(savedObject.timestamp).to.equal(newObject.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should accept an object for value', async () => {
      try {
        const newObject = new Obj({
          key: 'key',
          value: { a: 'A', b: 'B' },
          timestamp: 1514764800 // 2018-01-01 00:00:00
        });

        const savedObject = await newObject.save();

        expect(savedObject).to.be.an('object');
        expect(savedObject).to.have.property('_id');
        expect(savedObject.key).to.equal(newObject.key);
        expect(savedObject.value).to.equal(newObject.value);
        expect(savedObject.timestamp).to.equal(newObject.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should throw an error when key is not provided', async () => {
      try {
        const objWithoutKey = new Obj({
          value: '123'
        });

        await objWithoutKey.save();
        throw new Error('Expected to throw error');
      } catch (err) {
        expect(err.name).to.equal('ValidationError');
        expect(err.message).to.equal('object validation failed: key: Path `key` is required.');
      }
    });
  });

  describe('#findByLatestKey', () => {
    const objects = [
      {
        key: 'my-key-1',
        value: 'Jan 1, 2018',
        timestamp: 1514764800 // 2018-01-01 00:00:00
      },
      {
        key: 'my-key-1',
        value: 'Jan 2, 2018',
        timestamp: 1514851200 // 2018-01-02 00:00:00
      }
    ];

    beforeEach(async () => {
      try {
        await Obj.remove({}).exec();
        await Obj.create(objects);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should return the latest object if no timestamp is provided', async () => {
      try {
        const expected = objects[1];
        const result = await Obj.findByLatestKey(expected.key);

        expect(result).to.be.a('object');
        expect(result.key).to.equal(expected.key);
        expect(result.value).to.equal(expected.value);
        expect(result.timestamp).to.equal(expected.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should return the latest object earlier to the timestamp provided', async () => {
      try {
        const expected = objects[0];
        const timestamp = 1514764801; // 2018-01-01 00:00:01

        const result = await Obj.findByLatestKey(expected.key, timestamp);

        expect(result).to.be.a('object');
        expect(result.key).to.equal(expected.key);
        expect(result.value).to.equal(expected.value);
        expect(result.timestamp).to.equal(expected.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should return the latest object equal to the timestamp provided', async () => {
      try {
        const expected = objects[0];

        const result = await Obj.findByLatestKey(expected.key, expected.timestamp);

        expect(result).to.be.a('object');
        expect(result.key).to.equal(expected.key);
        expect(result.value).to.equal(expected.value);
        expect(result.timestamp).to.equal(expected.timestamp);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should return an empty array if key is not found', async () => {
      try {
        const result = await Obj.findByLatestKey('unknown-key');

        expect(result).to.equal(null);
      } catch (err) {
        throw new Error(err);
      }
    });

    it('should return an empty array if key with timestamp is not found', async () => {
      try {
        const key = objects[0].key;
        const timestamp = 1514764799; // 2017-12-31 23:59:59
        const result = await Obj.findByLatestKey(key, timestamp);

        expect(result).to.equal(null);
      } catch (err) {
        throw new Error(err);
      }
    });
  });

  describe('#formatForClient', () => {
    it('should remove _id and __v fields', async () => {
      try {
        const newObject = new Obj({
          key: 'ABC',
          value: '123'
        });

        const savedObject = await newObject.save();
        expect(savedObject).to.have.property('_id');
        expect(savedObject).to.have.property('__v');

        const sanitizedObject = savedObject.formatForClient();
        expect(sanitizedObject).to.not.have.property('_id');
        expect(sanitizedObject).to.not.have.property('__v');
      } catch (err) {
        throw new Error(err);
      }
    });
  });
});
