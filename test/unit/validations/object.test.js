/* global describe it */

const chai = require('chai');
const expect = chai.expect;
const utimestamp = require('unix-timestamp');
utimestamp.round = true;
const validations = require('../../../app/validations/object');

describe('Object', () => {
  describe('#getByKey', () => {
    const validate = validations.getByKey;

    describe('#getByKey.params', () => {
      it('should accept a string for `key`', () => {
        try {
          const req = {
            params: { key: 'key' }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should accept special characters for `key`', () => {
        try {
          const req = {
            params: { key: '我' }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should error out when `key` is a number', () => {
        try {
          const req = {
            params: { key: 1 }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should error out when `key` start with a `.`', () => {
        try {
          const req = {
            params: { key: '.string' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should error out when `key` start with a `$`', () => {
        try {
          const req = {
            params: { key: '$string' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should error out when `key` is blank', () => {
        try {
          const req = {
            params: { key: '' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should error out when `key` is blank after trimmed', () => {
        try {
          const req = {
            params: { key: '  ' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });
    });

    describe('#getByKey.query', () => {
      it('should accept a number for `timestamp`', () => {
        try {
          const req = {
            params: { key: 'key' },
            query: { timestamp: 0 }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should accept a query without `timestamp`', () => {
        try {
          const req = {
            params: { key: 'key' },
            query: {}
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should accept a numeric `timestamp` which is a string', () => {
        try {
          const req = {
            params: { key: 'key' },
            query: { timestamp: '1514764800' }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should return an error if `timestamp` is a decimal', () => {
        try {
          const req = {
            params: { key: 'key' },
            query: { timestamp: 1514764800.12 }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if `timestamp` is a negative', () => {
        try {
          const req = {
            params: { key: 'key' },
            query: { timestamp: -1514764800 }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if `timestamp` is greater than the current time', () => {
        try {
          const req = {
            params: { key: 'key' },
            query: { timestamp: utimestamp.now() + 5 } // 5s to the future
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });
    });
  });

  describe('#post', () => {
    describe('#post.body', () => {
      const validate = validations.post;

      it('should accept a single key-value pair', () => {
        try {
          const req = {
            body: { 'key': 'value' }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should accept a value which is a number', () => {
        try {
          const req = {
            body: { 'key': 1 }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should accept a value which is a array', () => {
        try {
          const req = {
            body: { 'key': ['1', '2'] }
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should accept a value which is a object', () => {
        try {
          const req = {
            body: { 'key': { arr: [1, 2, 3], obj: 1 }}
          };

          validate(req);
        } catch (err) {
          throw new Error(err);
        }
      });

      it('should return an error if multiple key-value pairs are given', () => {
        try {
          const req = {
            body: {
              key1: 'value1',
              key2: 'value2'
            }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if key is an empty string', () => {
        try {
          const req = {
            body: { ' ': 'value1' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if key is starts with a `.`', () => {
        try {
          const req = {
            body: { '.string': 'value1' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if key is starts with a `$`', () => {
        try {
          const req = {
            body: { '.$': 'value1' }
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if body is an array', () => {
        try {
          const req = {
            body: [1]
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });

      it('should return an error if body is a string', () => {
        try {
          const req = {
            body: 'string'
          };

          validate(req);
          throw new Error('Should throw error');
        } catch (err) {
          expect(err).is.not.equal(null);
          expect(err.name).is.equal('ValidationError');
        }
      });
    });
  });
});
