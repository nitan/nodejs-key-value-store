# Node.js key-value store

A version controlled key-value store implementation using Node.js


Current deployment can be accessed here:<br />
https://nodejs-key-value-store.herokuapp.com/

## Features
- Store a key-value pair
- Retrieve the latest key-value pair
- Retrieve key-value pair by timestamp
- CI/CD ready

## Limitations
Affected by the limitations of MongoDB, below are the app limitations:
- Key size should not exceed 1MB. See [MongoDB limit on indexes](https://docs.mongodb.com/manual/reference/limits/#Index-Key-Limit)
- Value size should not exceed 16MB. See [MongoDB limit on document size](https://docs.mongodb.com/manual/reference/limits/#bson-documents)
- Keys should not start with `$` or `.`. See [MongoDB naming restrictions](https://docs.mongodb.com/manual/reference/limits/#naming-restrictions)
- Keys for GET requests are assumed to be URL encoded (Percent-encoded) to be properly read

## Prerequisites
 - [Node v8.9+](https://nodejs.org/en/download/current/) or [Docker](https://www.docker.com/)
 - [Yarn](https://yarnpkg.com/en/docs/install)


## Getting Started
Clone the repo:

```bash
git clone https://gitlab.com/nitan/nodejs-key-value-store.git
```

Install dependencies:
```bash
yarn
```
Set environment variables:
```bash
export NODE_ENV=development;
export PORT=3000
export MONGODB_URI=mongodb://localhost:27017
```

## Running the code
You may choose to run the code locally or inside a docker container. Both methods **watch changes** made to the file

**Reminder:** When switching from method A to B, remove mongo container created in method A.

A. Locally

    1. docker run -d -p 27017:27017 --name mongo mongo
    2. yarn pm2-dev

B. Docker

    yarn run docker:dev

## Running the tests
You may choose to run the code locally or inside a docker container. Both methods watch changes made to the file.

**Reminder:** When switching from method A to B, remove mongo container created in method A.

A. Locally

    1. docker run -d -p 27017:27017 --name mongo mongo
    2. yarn test

B. Docker

    yarn run docker:test


## Linter
To run linter:

    yarn lint

To fix errors automatically:

    yarn lint:fix

## API Documentation
To generate API Documentation:

    yarn apidoc:update

To view documentation:

    1. Run the code
    2. Visit <server-url>/docs


<br />Current documentation can be found here:<br />
https://nodejs-key-value-store.herokuapp.com/docs

## Deployment to Production
Code is automatically deployed to heroku upon merge to master.
<br />ENV variables are set in Heroku config vars
<br />For steps on deployment, refer to `.gitlab-ci.yml`

## Future Improvements
- Authentication
- Linter in CI pipeline
- Support rollback of deployment
- Tests for Object size
