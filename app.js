const config = require('config');
const logger = require('./config/components/logger');
const app = require('./config/components/express');
const mongoose = require('./config/components/mongoose');

// Connect to MongoDB
const mongoURI = config.get('mongodb.uri');
const mongoOptions = config.get('mongodb.options');
mongoose.connect(mongoURI, mongoOptions);

// Start Server
const port = config.get('port');
app.listen(port, () => {
  logger.info(`Example app listening on port ${port}!`);
});

module.exports = app;
