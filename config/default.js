const path = require('path');

module.exports = {
  port: process.env.PORT || 3000,
  mongodb: {
    uri: process.env.MONGODB_URI || 'mongodb://localhost:27017',
    options: {}
  },
  winston: {
    level: 'info',
    transports: [
      {
        name: 'Console',
        options: {
          colorize: true,
          prettyPrint: true
        }
      }],
    exitOnError: false
  },
  apidoc: {
    dir: path.resolve(__dirname, '../docs')
  }
};
