module.exports = {
  winston: {
    level: 'error',
    transports: [
      {
        name: 'Console',
        options: {
          colorize: true,
          prettyPrint: true
        }
      }
    ],
    exitOnError: false
  }
};
