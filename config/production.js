module.exports = {
  winston: {
    level: 'info',
    transports: [
      {
        name: 'Console',
        options: {
          colorize: true,
          prettyPrint: true
        }
      },
      {
        name: 'File',
        options: {
          colorize: true,
          json: false,
          filename: 'service.log',
          maxsize: 1000000,
          maxFiles: 10,
          tailable: true,
          level: 'silly'
        }
      }
    ],
    exitOnError: false
  }
};
