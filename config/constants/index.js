module.exports = {
  'MONGODB_INDEX_MAX_SIZE': 1024,           // 1 KB
  'MONGODB_DOCUMENT_MAX_SIZE': 16777216     // 16 MB
};
