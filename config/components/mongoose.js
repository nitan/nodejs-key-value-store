const mongoose = require('mongoose');
const logger = require('./logger');

/*
 * Connect to MongoDB
 */
const connect = (uri, options) => {
  mongoose.connect(uri, options);
  return mongoose.connection;
};

// Exit application on error
mongoose.connection.on('error', (err) => {
  logger.error(`MongoDB connection error: ${err}`);
  process.exit(-1);
});

// Print logs on dev
if (process.env.NODE_ENV === 'development') {
  mongoose.set('debug', true);
}

module.exports = {
  connect
};
