const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const logger = require('./logger');
const routes = require('../../app/routes/v1');
const error = require('../../app/middlewares/error');

const app = express();

// Convert body to JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// API Logger Middleware
app.use(morgan('combined', { stream: logger.stream }));

// APIDoc Endpoint
const apiDocDir = config.get('apidoc.dir');
app.use('/docs', express.static(apiDocDir));

// Routes from v1
app.use('/', routes);

// Error Handlers
app.use(error.notFound);
app.use(error.validation);
app.use(error.handler);

module.exports = app;
