const config = require('config');
const winston = require('winston');
const _ = require('lodash');

let winstonConfig = config.get('winston');

// Add transports
// Workaround: Using `config` module causes errors when specifying Transport in config
// Set transport here instead.
winstonConfig = _.cloneDeep(winstonConfig);
winstonConfig.transports = winstonConfig.transports.map((transport) => {
  let Transport = winston.transports[transport.name];
  let options = transport.options || {};
  return new Transport(options);
});

// Configure Logger
var logger = winston.createLogger(winstonConfig);

// Stream object to be used by 'morgan'
logger.stream = {
  write: (message, encoding) => {
    logger.info(message);
  }
};

module.exports = logger;
