FROM node:8.9.0-alpine

EXPOSE 3000

RUN mkdir /app
WORKDIR /app
RUN npm install -g pm2
ADD package.json yarn.lock  /app/
RUN yarn install --pure-lockfile
ADD . /app
RUN yarn apidoc:update

CMD ["pm2-runtime", "app.js"]
