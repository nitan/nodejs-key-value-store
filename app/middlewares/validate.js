const _ = require('lodash');

module.exports = (validateFunction) => {
  return (req, res, next) => {
    try {
      const reqObj = _.pick(req, ['params', 'query', 'header', 'body']);
      validateFunction(reqObj);
      next();
    } catch (err) {
      next(err);
    }
  };
};
