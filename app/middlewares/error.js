const httpStatus = require('http-status');
const APIError = require('../errors/APIError');
const ValidationError = require('../errors/ValidationError');
const logger = require('../../config/components/logger');

/*
 * Format Validation errors from express-validator
 */
const validation = (err, req, res, next) => {
  if (err instanceof ValidationError) {
    err.status = httpStatus.BAD_REQUEST;
  }
  next(err);
};

/*
 * Format Validation errors from express-validator
 */
const notFound = (req, res, next) => {
  next(new APIError.NotFound('Route not found'));
};

/*
 * Handle all error codes
 */
const handler = (err, req, res, next) => {
  logger.error(err);

  const response = {
    code: err.status || httpStatus.INTERNAL_SERVER_ERROR,
    message: err.message,
    errors: err.errors,
    stack: err.stack
  };

  if (process.env.NODE_ENV !== 'development') {
    delete response.stack;
  }

  res.status(response.code);
  res.json(response);
  res.end();
};

module.exports = {
  validation,
  notFound,
  handler
};
