const mongoose = require('mongoose');
const utimestamp = require('unix-timestamp');
utimestamp.round = true;

var objectSchema = new mongoose.Schema({
  key: {
    type: String,
    trim: true,
    index: true,
    required: true
  },
  value: {
    type: Object
  },
  timestamp: {
    type: Number
  }
});

/*
 * Sets the timestamp to current unix timestamp
 */
objectSchema.pre('save', function (next) {
  if (!this.timestamp) {
    this.timestamp = utimestamp.now();
  }

  next();
});

/*
 * Find object by latest key
 */
objectSchema.statics.findByLatestKey = async function (key, timestamp = utimestamp.now()) {
  const result = await this.find({
    key: key,
    timestamp: { $lte: timestamp }
  }).sort({
    timestamp: -1
  }).limit(1)
    .exec();

  if (result.length === 0) {
    return null;
  };

  return result[0];
};

/*
 * Format object for client by sanitizing data
 */
objectSchema.method('formatForClient', function () {
  let object = this.toObject();

  delete object._id;
  delete object.__v;

  return object;
});

const object = mongoose.model('object', objectSchema);

module.exports = object;
