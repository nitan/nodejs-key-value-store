const Joi = require('joi');
const _ = require('lodash');
const Bson = require('bson');
const BSON = new Bson();

const ValidationError = require('../errors/ValidationError');
const constants = require('../../config/constants');
const utimestamp = require('unix-timestamp');
utimestamp.round = true;

const getByKey = (req) => {
  const schema = {
    params: {
      key: Joi.string().trim().regex(/^(?!(\.|\$))./).required()
    },
    query: {
      timestamp: Joi.number().integer().min(0)
    }
  };

  const reqValidated = validateByJoi(req, schema);

  // Validate timestamp
  const timestamp = _.get(reqValidated, 'query.timestamp');
  validateTimestamp(timestamp);
};

const post = (req) => {
  // Validate Body
  const bodySchema = {
    body: Joi.object().length(1)
  };

  const bodyValidated = validateByJoi(req, bodySchema);

  // Validate Key
  const keySchema = {
    key: Joi.string().trim().regex(/^(?!(\.|\$))./).required()
  };

  const body = bodyValidated.body;
  const keyName = Object.keys(body)[0];
  const key = {
    key: keyName
  };

  validateByJoi(key, keySchema);

  // Validate Sizes
  validateKeySize(keyName);
  validateValueSize(body[keyName]);
};

const validateTimestamp = (timestamp) => {
  const currentTime = utimestamp.now();
  if (timestamp && timestamp > currentTime) {
    throw new ValidationError(`Timestamp should be less than current time ${currentTime}`);
  }
};

const validateKeySize = (key) => {
  const KB1 = constants.MONGODB_INDEX_MAX_SIZE;

  if (BSON.calculateObjectSize(key) > KB1) {
    throw new ValidationError('Size of `key` should not be greater than 1 KB');
  }
};

const validateValueSize = (value) => {
  const MB16 = constants.MONGODB_DOCUMENT_MAX_SIZE;

  if (BSON.calculateObjectSize(value) > MB16) {
    throw new ValidationError('Size of `key` should not be greater than 16 MB');
  }
};

const validateByJoi = (actual, expected) => {
  const result = Joi.validate(actual, expected, {stripUnknown: true});

  if (result.error) {
    throw new ValidationError(result.error.details);
  }

  return result.value;
};

module.exports = {
  getByKey,
  post
};
