class ValidationError extends Error {
  constructor (errors = undefined) {
    const message = 'ValidationError';

    super(message);
    Error.captureStackTrace(this);

    this.name = this.constructor.name;
    this.message = message;
    this.errors = errors;
  }
};

module.exports = ValidationError;
