const httpStatus = require('http-status');

class APIError extends Error {
  constructor (
    HTTPCode = httpStatus.INTERNAL_SERVER_ERROR,
    message = 'ServerError',
    errors = undefined
  ) {
    super(message);
    Error.captureStackTrace(this);

    this.message = message;
    this.errors = errors;
    this.status = HTTPCode;
  }
}

class NotFound extends APIError {
  constructor (
    errors = undefined
  ) {
    super(httpStatus.NOT_FOUND, 'NotFound', errors);
  }
}

module.exports = {
  APIError,
  NotFound
};
