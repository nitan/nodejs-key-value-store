const express = require('express');
const router = express.Router();

const object = require('./object');

router.use('/object', object);

module.exports = router;
