const express = require('express');
const validate = require('../../middlewares/validate');
const controller = require('../../controllers/object');
const validations = require('../../validations/object');

const router = express.Router();

/**
 * @api {get} /object/:key Get Latest Key
 * @apiVersion 1.0.0
 * @apiName GetObjectByKey
 * @apiGroup Object
 *
 * @apiDescription Accept a key and returns the corresponding latest value
 *
 * @apiParam (Param) {string} key Key of object to be returned.
 * <br />Should not start with a `$` or `.`
 * <br />Should be URI encoded (Percent-encoded)
 * @apiParam (Query) {Number} [timestamp=now()] Unix timestamp
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *   {
 *     "key": "my-key-1",
 *     "value": "my-value-1",
 *     "timestamp": 1514764800
 *    }
 *
 *  @apiError {Object} ValidationError Query is invalid (e.g. Timestamp greater than current time, Invalid Key)
 *  @apiError {Object} NotFound Key with timestamp does not exist
 */
router.get('/:key', validate(validations.getByKey), controller.getByKey);

/**
 * @api {post} /object Create a key
 * @apiVersion 1.0.0
 * @apiName PostObject
 * @apiGroup Object
 *
 * @apiDescription Create a new key-value pair
 *
 * @apiParam (Body) {Object} any Object with a single key-value pair (e.g. { key : value }).
 * <br />Key should not start with a with a `$` or `.`
 * <br />Key size should not exceed 1KB
 * <br />Value size should not exceed 16MB
 *
 * @apiParamExample {json} Request-Example:
 *       {"key" : "value"}
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 201 CREATED
 *   {
 *     "key": "key",
 *     "value": "value",
 *     "timestamp": 1514764800
 *    }
 *
 * @apiError {Object} ValidationError Request body is invalid
 */
router.post('/', validate(validations.post), controller.post);

module.exports = router;
