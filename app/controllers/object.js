const httpStatus = require('http-status');
const Obj = require('../models/object');
const Error = require('../errors/APIError');

const getByKey = async (req, res, next) => {
  try {
    const key = req.params.key;
    const timestamp = req.query.timestamp;

    const object = await Obj.findByLatestKey(key, timestamp);

    if (!object) {
      throw new Error.NotFound(`Object with key: ${key} not found`);
    }

    res.send(object.formatForClient());
  } catch (err) {
    next(err);
  }
};

const post = async (req, res, next) => {
  try {
    const body = req.body;
    const key = Object.keys(body)[0];

    const object = {
      key: key,
      value: body[key]
    };

    const newObject = new Obj(object);
    const savedObject = await newObject.save();

    res.status(httpStatus.CREATED);
    res.send(savedObject.formatForClient());
  } catch (err) {
    next(err);
  }
};

module.exports = {
  getByKey,
  post
};
